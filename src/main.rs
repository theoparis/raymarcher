use mtpng::{
	encoder::{Encoder, Options},
	ColorType, Header,
};
use std::time::Instant;
use tracing::debug;
use vulkano::{
	buffer::{BufferUsage, CpuAccessibleBuffer},
	command_buffer::{AutoCommandBufferBuilder, CommandBufferUsage},
	descriptor_set::{PersistentDescriptorSet, WriteDescriptorSet},
	device::{
		physical::PhysicalDevice, Device, DeviceCreateInfo, DeviceExtensions,
		QueueCreateInfo,
	},
	format::Format,
	image::{view::ImageView, ImageDimensions, StorageImage},
	instance::Instance,
	pipeline::{ComputePipeline, Pipeline, PipelineBindPoint},
	sync::{self, GpuFuture},
};

fn main() {
	tracing_subscriber::fmt::init();
	let width = 1920;
	let height = 1080;

	let instance = Instance::new(Default::default()).unwrap();

	let device_extensions = DeviceExtensions {
		khr_storage_buffer_storage_class: true,
		..DeviceExtensions::none()
	};
	let (physical_device, queue_family) = PhysicalDevice::enumerate(&instance)
		.filter(|&p| {
			p.supported_extensions().is_superset_of(&device_extensions)
		})
		.filter_map(|p| {
			// The Vulkan specs guarantee that a compliant implementation must provide at least one queue
			// that supports compute operations.
			p.queue_families()
				.find(|&q| q.supports_compute())
				.map(|q| (p, q))
		})
		.find(|(p, _)| {
			p.properties()
				.device_name
				.to_ascii_lowercase()
				.contains(&std::env::var("GPU").unwrap())
		})
		// .min_by_key(|(p, _)| match p.properties().device_type {
		// 	PhysicalDeviceType::DiscreteGpu => 0,
		// 	PhysicalDeviceType::IntegratedGpu => 1,
		// 	PhysicalDeviceType::VirtualGpu => 2,
		// 	PhysicalDeviceType::Cpu => 3,
		// 	PhysicalDeviceType::Other => 4,
		// })
		.unwrap();

	debug!(
		"Using device: {} (type: {:?})",
		physical_device.properties().device_name,
		physical_device.properties().device_type
	);

	let (device, mut queues) = Device::new(
		physical_device,
		DeviceCreateInfo {
			enabled_extensions: physical_device
				.required_extensions()
				.union(&device_extensions),
			queue_create_infos: vec![QueueCreateInfo::family(queue_family)],
			..Default::default()
		},
	)
	.unwrap();

	let queue = queues.next().unwrap();

	let pipeline = {
		#[allow(clippy::needless_question_mark)]
		mod cs {
			vulkano_shaders::shader! {
				ty: "compute",
				src: "
                    #version 450
                    layout(local_size_x = 8, local_size_y = 8, local_size_z = 1) in;
					layout(set = 0, binding = 0, rgba8) uniform writeonly image2D img;
					
					int maxSteps = 100;
					float maxDist = 100;
					float surfaceDist = 0.01;
					
					float getDist(vec3 p) {
						vec4 s = vec4(0, 1, 6, 1);

						float sphereDist = length(p - s.xyz) - s.w;
						float planeDist = p.y;

						float d = min(sphereDist, planeDist);

						return d;
					}

					float rayMarch(vec3 ro, vec3 rd) {
						float distOrigin = 0.0;

						for (int i = 0; i < maxSteps; i++) {
							vec3 p = ro + distOrigin * rd;
							float ds = getDist(p);
							
							distOrigin += ds;

							if (ds < surfaceDist || distOrigin > maxDist) break;
						}

						return distOrigin;
					}

                    void main() {
						vec2 dimensions = vec2(imageSize(img));
						vec2 uv = (gl_GlobalInvocationID.xy - 0.5 * dimensions.xy) / dimensions.y;
					
						vec3 ro = vec3(0, 1, 0);
						vec3 rd = normalize(vec3(uv.x, uv.y, 1));

						float d = rayMarch(ro, rd);
						d /= 6.0;
						vec4 color = vec4(vec3(d), 1.0);

                        imageStore(img, ivec2(gl_GlobalInvocationID.xy), color);
                    }
                "
			}
		}
		let shader = cs::load(device.clone()).unwrap();
		ComputePipeline::new(
			device.clone(),
			shader.entry_point("main").unwrap(),
			&(),
			None,
			|_| {},
		)
		.unwrap()
	};

	let image = StorageImage::new(
		device.clone(),
		ImageDimensions::Dim2d {
			width,
			height,
			array_layers: 1,
		},
		Format::R8G8B8A8_UNORM,
		Some(queue.family()),
	)
	.unwrap();

	let view = ImageView::new_default(image.clone()).unwrap();

	let layout = pipeline.layout().set_layouts().get(0).unwrap();
	let set = PersistentDescriptorSet::new(
		layout.clone(),
		[WriteDescriptorSet::image_view(0, view)],
	)
	.unwrap();

	let buf = CpuAccessibleBuffer::from_iter(
		device.clone(),
		BufferUsage::all(),
		false,
		(0..width * height * 4).map(|_| 0u8),
	)
	.expect("failed to create buffer");

	let mut builder = AutoCommandBufferBuilder::primary(
		device.clone(),
		queue.family(),
		CommandBufferUsage::OneTimeSubmit,
	)
	.unwrap();

	let start = Instant::now();

	builder
		.bind_pipeline_compute(pipeline.clone())
		.bind_descriptor_sets(
			PipelineBindPoint::Compute,
			pipeline.layout().clone(),
			0,
			set,
		)
		.dispatch([width / 8, height / 8, 1])
		.unwrap()
		.copy_image_to_buffer(image, buf.clone())
		.unwrap();

	let command_buffer = builder.build().unwrap();

	let future = sync::now(device)
		.then_execute(queue, command_buffer)
		.unwrap()
		.then_signal_fence_and_flush()
		.unwrap();

	future.wait(None).unwrap();

	let buffer_content = buf.read().unwrap();
	let end = start.elapsed();
	//let image = ImageBuffer::<Rgba<u8>, _>::from_raw(
	//width,
	//height,
	//&buffer_content[..],
	//)
	//.unwrap();
	//image.save("image.png").unwrap();

	let writer = Vec::<u8>::new();

	let mut header = Header::new();
	header.set_size(width, height).unwrap();
	header.set_color(ColorType::TruecolorAlpha, 8).unwrap();

	let options = Options::new();

	let mut encoder = Encoder::new(writer, &options);

	encoder.write_header(&header).unwrap();
	encoder.write_image_rows(&buffer_content[..]).unwrap();
	encoder.finish().unwrap();

	tracing::info!("Took {} seconds", end.as_secs_f64());
	tracing::info!("Completed render saved to 'image.png'");
}
